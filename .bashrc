#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
export PS1="\[\e[0;35m\] \W/ ~>>\[\e[m\] "

# Custom scripts in PATH
# PATH="/home/xampinyon/.local/scripts:${PATH}"
# export PATH
# export EDITOR="vim"
# export TERM="termite"



#    ___    ___                     
#   /   |  / (_)___ _________  _____
#  / /| | / / / __ `/ ___/ _ \/ ___/
# / ___ |/ / / /_/ (__  )  __(__  ) 
#/_/  |_/_/_/\__,_/____/\___/____/  

# git control dels dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

# previsualització imatges lf
alias lf="lfub"

# Navigation
alias ide="cd ~/Documents/idees_en_si/ && lf"
alias dow="cd ~/Downloads/ && lf"
alias boox="cd ~/Documents/boox && lf"

alias uni="cd ~/Documents/uni/ && lf"

alias alg="cd ~/Documents/uni/algebra"
alias ana="cd ~/Documents/uni/analisi"
alias edo="cd ~/Documents/uni/equacions_diferencials"
alias est="cd ~/Documents/uni/estructures"
alias met="cd ~/Documents/uni/metodes"
alias pro="cd ~/Documents/uni/programacio"
alias top="cd ~/Documents/uni/topologia"


##### lfcd #######
#LFCD="/home/xampinyon/.config/lf/scripts/lfcd.sh"
#if [ -f "$LFCD" ]; then
#    source "$LFCD"
#fi

lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir"
            fi
        fi
    fi
}

bind '"\C-o":"lfcd\C-m"'  # bash
