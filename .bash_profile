#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Custom scripts in PATH
PATH="/home/xampinyon/.local/scripts:${PATH}"
PATH="/home/xampinyon/.local/bin:${PATH}"
export PATH
export EDITOR="vim"
export TERMINAL="alacritty"
export LS_COLORS="di=1;35:ln=1;36:so=1;32:pi=1;33:ex=1;31:bd=1;34;46:cd=1;34;47:su=1;30;41:sg=1;30;46:tw=1;30;42:ow=1;30;43"



if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
       exec startx
fi       
