import subprocess
import os
import dracula.draw
from qutebrowser.api import interceptor

config.load_autoconfig()
c.statusbar.show = "in-mode"

# =============== Minimize fingerprinting ===============
c.content.headers.user_agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0"
c.content.headers.accept_language = "en-Us,en;q=0.5"
c.content.headers.custom = {"accept": "text/html,application/xhtml-xml,application/xml;q=0.9,*/*;q=0.8"}

# =============== Youtube adblocker ===============
def filter_yt(info: interceptor.Request):
    """ Block the given request if necessary."""
    url = info.request_url
    if (
        url.host() == "www.youtube.com"
        and url.path() == "/get_video_info"
        and "&adformat=" in url.query()
    ):
        info.block()

interceptor.register(filter_yt)

# =============== Dracula theme ===============

dracula.draw.blood(c, {
    'spacing': {
        'vertical':6,
        'horizontal':8
    }
})

# =============== Dark mode pages ===============

c.colors.webpage.preferred_color_scheme = "dark"
