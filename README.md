# Configuració

Els meus arxius de configuració.


## Programes

- vim
- lf
- termite
- polybar
- mpd
- ncmpcpp

- spicetify: el theme de https://github.com/NYRI4/Comfy-spicetify
