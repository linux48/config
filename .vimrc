call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'Rigellute/shades-of-purple.vim'
Plug 'vim-airline/vim-airline'
call plug#end()

filetype plugin indent on

" Posar els numerets de l'esquerra
	set number relativenumber
" Autocompletion o com es diga
	set wildmode=longest,list,full
" Split baix o l'esquerre
	set splitbelow splitright
" Activar sintaxis y posar el color, també el encoding
	syntax on
	set encoding=utf-8
	colorscheme desert 
" Desactivar el autocomment en línia nova
	autocmd Filetype * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
	let g:shades_of_purple_airline = 1
	let g:airline_theme='shades_of_purple'
	let g:airline_powerline_fonts = 1

" Opcions que son molt benvingudes
" Better command-line completion
	set wildmenu

" Show partial commands in the last line of the screen
	set showcmd

" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
	set hlsearch

" Use case insensitive search, except when using capital letters
	set ignorecase
	set smartcase

" Allow backspacing over autoindent, line breaks and start of insert action
	set backspace=indent,eol,start
" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
	set confirm
" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
	set shiftwidth=4
	set softtabstop=4
	set expandtab


" Autocompilar el latex
    autocmd BufWritePost *.tex silent! execute "!pdflatex % >/dev/null 2>&1" | redraw!


"------------------------------------------------------------
" Mappings {{{1
"
" Useful mappings

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$

" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-L> :nohl<CR><C-L>


" --------- LaTeX mappings --------
inoremap <Space><Space> <Esc>/<++><Enter>"_c4l
nnoremap <Space><Space> <Esc>/<++><Enter>"_c4l

autocmd Filetype tex inoremap ;bf \textbf{}<Space><++><Esc>T{i
autocmd Filetype tex inoremap ;it \textit{}<Space><++><Esc>T{i
autocmd Filetype tex inoremap ;tt \texttt{}<Space><++><Esc>T{i
autocmd Filetype tex inoremap ;em \emph{}<Space><++><Esc>T{i
autocmd Filetype tex inoremap ;bb \mathbb{}<Space><++><Esc>T{i
autocmd Filetype tex inoremap ;cal \mathcal{}<Space><++><Esc>T{i
autocmd Filetype tex inoremap ;d $$<Space><++><Esc>T$hi

autocmd Filetype tex inoremap ;beg \begin{<++>}<Enter><++><Enter>\end{<++>}<Enter><Enter><++><Esc>4kfR<Space><Space>
autocmd Filetype tex inoremap ;mat \begin{pmatrix}<Space><Space>\end{pmatrix}<Space><++><Esc>18hi
autocmd Filetype tex inoremap ;sec \section{}<Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;p \paragraph{}<Space><++><Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;blue {\color{blue}<Space>}<Esc>i
autocmd Filetype tex inoremap ;it \begin{itemize}<Enter><Tab>\item<Space><Enter>\end{itemize}<Esc><<o<Enter><++><Esc>3k$a
autocmd Filetype tex inoremap ;en \begin{enumerate}<Enter><Tab>\item<Space><Enter>\end{enumerate}<Esc><<o<Enter><++><Esc>3k$a
autocmd Filetype tex inoremap ;cen \begin{center}<Enter><Enter>\end{center}<Enter><Enter><++><Esc>3ki<Tab>
autocmd Filetype tex inoremap ;ver \begin{verbatim}<Enter><Enter>\end{verbatim}<Enter><Enter><++><Esc>3ki<Tab>
autocmd Filetype tex inoremap ;tab \begin{table}[htbp]<Enter><Tab>\centering<Enter>\begin{tabular}{}<Enter><Tab>\hline<Enter><++><Enter>\hline<Enter><++><Enter>\hline<Enter><BS>\end{tabular}<Enter>\caption{<++>}<Enter>\label{t:<++>}<Enter><BS>\end{table}<Enter><Enter><++><Esc>11kf}f}i
autocmd Filetype tex inoremap ;eq \begin{equation}<Enter><Enter>\label{eq:<++>}<Enter>\end{equation}<Enter><Enter><++><Esc>4ki<Tab>
autocmd Filetype tex inoremap ;fig \begin{figure}[htbp]<Enter><Tab>\centering<Enter>\includegraphics[width=\textwidth]{}<Enter>\caption{<++>}<Enter>\label{fig:<++>}<Enter><Esc><<i\end{figure}<Enter><Enter><++><Esc>5kf}i


"------------------------------------------------------------

